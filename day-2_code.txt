#here we import the input.txt file

from google.colab import files
input_filepath=str(files.upload())       

#given information about count of countries and balance amount etc.

grids_size=10
max_countries=20
Each_country_intial_balance=1000000
partion_exchange=1000          # to get a euro coin of other country we give 1000 coins in our country


#to read the input.txt file

def read_file(filepath):
    data=open('input.txt','r').read()
    return data.split('\n')
print(read_file(input_filepath))


#checking of validity of input

def is_valid_input():
        cases_list = []
        lines = read_file(input_filepath)
        line_index = 0 
        case = 0
        while  line_index < len(lines):
             countries_len = int(lines[0])            
             #countries_len = int(countries_len)            
             if countries_len == 0:
                return cases_list
             #print(type(countries_len)) 
             #print(type(max_countries))
             country_len=int(countries_len)
             if country_len > max_countries or countries_len < 1:
                raise Exception("Error in input for case %i: invalid amount of countries" % (case + 1))
             line_index += 1
        return line_index
        
print(is_valid_input())
  
