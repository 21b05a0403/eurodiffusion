

#checking of validity of input
from collections import OrderedDict
def is_valid_input():
        cases_list = []
        lines = read_file(input_filepath)
        line_index = 0 
        case = 0
        while  line_index < len(lines):
             countries_len = int(lines[line_index])            
             #countries_len = int(countries_len)            
             if countries_len == 0:
                return cases_list
             #print(type(countries_len)) 
             #print(type(max_countries))
             country_len=int(countries_len)
             if country_len > max_countries or countries_len < 1:
                raise Exception("Error in input for case %i: invalid amount of countries" % (case + 1))
             line_index += 1
             countries_list = []
             for j in range(countries_len):
                ordered = is_valid_country(lines[line_index])
                print(ordered)
                countries_list.append(ordered)
                line_index += 1

                #ordered = is_valid_country(lines[line_index])
                #print(ordered)
                access_country = country_name(lines[line_index])
                print(access_country)
                length_country = No_of_cities(ordered['name'], ordered['sw']['xl'], ordered['sw']['yl'],
                                              ordered['ne']['xh'],
                                              ordered['ne']['yh'])

                print(len(length_country.cities))

        case += 1
        cases_list.append(countries_list)
        return cases_list

print(is_valid_input())  


#This function will return names of the given countries as country_list
def country_name(line):
    y = line.split(' ')
    country_list = []
    for j in range(1):
        z = y[j]
        country_list.append(z)
    return (country_list)


#This function will return the co-ordinates given 
def coordinates_list(line):
    args = str(line).split(' ')
    coordinates = []
    for i in range(1, 5):
        x = args[i]
        coordinates.append(x)
    return coordinates
#lines=read_file(input_filepath)    
#print(coordinates_list(lines[2]))


#this function will give us the country is valid or not
def is_valid_country(line):
    args = str(line).split(' ')
    print(args)
    if len(args) != 5 :
        raise Exception("Given input is invalid")
    for i in range(1, 5):
        if int(args[i]) <= 0 or int(args[i]) >= (grids_size + 1):
            raise Exception("Error at line {%s}: invalid country coordinates" % line)

    country = {
        "name": args[0],
        "sw": {
            "xl": int(args[1]),
            "yl": int(args[2])
        },
        "ne": {
            "xh": int(args[3]),
            "yh": int(args[4])
        }
    }

    return country
#lines=read_file(input_filepath)    
#print(is_valid_country(lines[3]))

#Here we create a class to in the co-ordinates of nw and se cites which gives the no.of cites in the country 
  
class City:
    def __init__(self, country, x: int, y: int):
        self.country: 'Country' = country
        self.x = x
        self.y = y
        self.balance = 0
class No_of_cities:

    def __init__(self, name: str, xl: int, yl:int, xh: int, yh:int):
        self.name = name
        self.cities = [
                        City(self, x, y)
                        for y in range(yl, yh + 1)
                        for x in range(xl, xh + 1)
                    ]



#Gives co-ordinates of all cities in the country
 
lines = read_file(input_filepath)
#lines=lines1.pop()
print(lines)
print(len(lines))
line_index = 0
case = 0
while line_index < len(lines)-1:
    countries_len = (lines[line_index])
    #print(type(countries_len))
    print(countries_len)

    #line_index += 1

    countries_list = []
    for j in range(1, int(countries_len) + 1):
        coordinates = coordinates_list(lines[j])
        x_coordinates = list(range(int(coordinates[0]), int(coordinates[2]) + 1))
        y_coordinates = list(range(int(coordinates[1]), int(coordinates[3]) + 1))
        line_index += 1
        print([(x,y) for x in x_coordinates for y in y_coordinates])
    line_index += 1
    case += 1

size =10
print(case)




      